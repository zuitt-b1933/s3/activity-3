package com.zuitt.batch193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WDC043_S3_A3 {
    public static void main(String[] args) {

        int numFactorial = 1;
        int i = 1;
        int num = 0;

        Scanner in = new Scanner(System.in);
        try{
            System.out.println("Input an Integer whose factorial is to be computed: ");
             num = in.nextInt();
        }catch (InputMismatchException e){
            System.out.println("You must enter a number to proceed.");
        }catch (Exception e){
            System.out.println("Invalid Input");
        }
            while (i <= num) {
                numFactorial = numFactorial * i;
                i++;
            }
            System.out.println("Factorial of " + num + " is " + numFactorial);
    }
}
